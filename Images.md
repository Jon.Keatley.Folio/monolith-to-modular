### Slide 2 - Monolith

```mermaid
graph TD
    subgraph Monolith[ ]
        UI---auth[Authentication]
        DataAccess[Data Access]
        auth---Accounts --- DataAccess
        auth---Products --- DataAccess
        auth---Promotions --- DataAccess
        auth---Orders --- DataAccess
    end
    Client ---UI
    DataAccess --- db[(Data Stores)]
```
### Slide 3 Modular

```mermaid
graph TD
    ClientA[Client A]  --- Components
    ClientB[Client B]  --- Components
    ClientC[Client C]  --- Components
    Components[Web Components] --- lb[Load balancer]
    lb --- apig[API Gateway / Backend for Frontend]
    subgraph SaaS
        SSO --- LDAP
    end
    Components --- SSO
    SSO --- apig
    subgraph hscale[Horizontally scaled on demand]
        apig -- Stateless --- redis[Caching - Redis / Memcache]
        apig -- JWT --- Accounts
        apig -- JWT --- Orders
        Products
        Promotions
    end
    redis --- Products
    redis --- Promotions
    Accounts --- adb[(Accounts DB)]
    Products --- proddb[(Products DB)]
    Promotions --- promdb[(Promotions DB)]
    Orders --- orddb[(Orders DB)]
    orddb -- CDC ---> RabbitMQ
    adb -- CDC ---> RabbitMQ
    RabbitMQ -- Updates --> orddb
    RabbitMQ -- Updates --> adb
    RabbitMQ -- Populates --> proddb
    RabbitMQ -- Populates --> promdb
    RabbitMQ --- business[Internal systems]
```
