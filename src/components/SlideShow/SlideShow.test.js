import { render, screen } from '@testing-library/react';
import SlideShow from './SlideShow';

test('renders TODO text', () => {
  render(<SlideShow author="Steve" />);
  const linkElement = screen.getByText(/Steve/i);
  expect(linkElement).toBeInTheDocument();
});
