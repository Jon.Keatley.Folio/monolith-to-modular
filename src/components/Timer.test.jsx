import { render, screen } from '@testing-library/react';
import Timer from './Timer';

test('Check seconds', () => {
  render(<Timer seconds="32" />);
  const linkElement = screen.getByText(/32/i);
  expect(linkElement).toBeInTheDocument();
});

test('Check minutes', () => {
  render(<Timer minutes="2" />);
  const linkElement = screen.getByText(/2:0/i);
  expect(linkElement).toBeInTheDocument();
});
