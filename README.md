# monolith-to-modular

A short web app based presentation about **“Migrating a legacy monolith web application to a modern modular application”**

See it live [here](https://slideshow.joncorp.org)

## Slides

| Slide | Name | purpose | status |
|-------|------|---------|--------|
| 1  | intro | Opening page - Introduces the theme and myself | 70% - needs animations |
| 2  | morph-mono | Detail the key elements of a monolith web application | 100% |
| 3  | morph-many | Detail the key elements of a modern modular web application | 100% |
| 4  | why-change | Detail the reasons for wanting to move ... | 100% - slide is boring to look at |
| 5  | change-how | Detail the different options for migrating highlighting the option I will focus on | 100% - needs images |
| 6  | change-plan | High level migration plan. |100% |
| ?  | questions   | Placeholder for people to ask questions | 100% |
| ?  | links       | Any useful links I come across | 100% |

## Missing

- Not enough unit tests
- Improved linting
- Not enough animation
- Style could be improved
- Count down timer rather than count up

## Packages

- https://create-react-app.dev/docs/getting-started/
- https://github.com/feathericons/react-feather
- https://react-spring.dev/

## Inspiration

- https://codepen.io/bcarvalho/pen/WXmwBq


# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Additional Scripts

### `npm testci`

Same fresh flavour as `npm test` but with `CI=true` at the start to disable the CLI interface loop.

### `npm lint:scripts`


Runs `eslint` against all `.js` and `.jsx` files found in `src/**/*`. Need to work on this as it is currently far too permissive.

---

# More from `create-react-app`

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
